===========================
Purchase Complaint Scenario
===========================

Imports::

    >>> import datetime
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard, Report
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import (
    ...     create_company, get_company)
    >>> from trytond.modules.account.tests.tools import (
    ...     create_fiscalyear, create_chart, get_accounts)
    >>> from trytond.modules.account_invoice.tests.tools import (
    ...     set_fiscalyear_invoice_sequences)
    >>> today = datetime.date.today()

Activate modules::

    >>> config = activate_modules('purchase_complaint')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']

Create supplier::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()

Create complaint type::

    >>> Type = Model.get('purchase.complaint.type')
    >>> IrModel = Model.get('ir.model')
    >>> purchase_type = Type(name='Purchase')
    >>> purchase_type.origin, = IrModel.find(
    ...     [('model', '=', 'purchase.purchase')])
    >>> purchase_type.save()
    >>> purchase_line_type = Type(name='Purchase Line')
    >>> purchase_line_type.origin, = IrModel.find(
    ...     [('model', '=', 'purchase.line')])
    >>> purchase_type.save()
    >>> purchase_line_type.save()
    >>> invoice_type = Type(name='Invoice')
    >>> invoice_type.origin, = IrModel.find(
    ...     [('model', '=', 'account.invoice')])
    >>> invoice_type.save()
    >>> invoice_line_type = Type(name='Invoice Line')
    >>> invoice_line_type.origin, = IrModel.find(
    ...     [('model', '=', 'account.invoice.line')])
    >>> invoice_line_type.save()

Create account category::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')

    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.purchasable = True
    >>> template.list_price = Decimal('10')
    >>> template.account_category = account_category
    >>> product, = template.products
    >>> product.cost_price = Decimal('5')
    >>> template.save()
    >>> product, = template.products

Configure locations::

    >>> Location = Model.get('stock.location')
    >>> lost_found_loc, = Location.find([('type', '=', 'lost_found')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> warehouse_loc, = Location.find([('type', '=', 'warehouse')])
    >>> warehouse_loc.waste_locations.append(lost_found_loc)
    >>> warehouse_loc.save()

Purchase 5 products::

    >>> Purchase = Model.get('purchase.purchase')
    >>> purchase = Purchase()
    >>> purchase.party = supplier
    >>> purchase_line = purchase.lines.new()
    >>> purchase_line.product = product
    >>> purchase_line.quantity = 3
    >>> purchase_line = purchase.lines.new()
    >>> purchase_line.product = product
    >>> purchase_line.quantity = 2
    >>> purchase.click('quote')
    >>> purchase.click('confirm')
    >>> purchase.click('process')
    >>> purchase.untaxed_amount
    Decimal('25.00')

Post the invoice::

    >>> invoice, = purchase.invoices
    >>> invoice.invoice_date = today
    >>> invoice.click('post')

Create a complaint to return the purchase::

    >>> Complaint = Model.get('purchase.complaint')
    >>> complaint = Complaint()
    >>> complaint.supplier = supplier
    >>> complaint.type = purchase_type
    >>> complaint.origin = purchase
    >>> action = complaint.actions.new()
    >>> action.action = 'purchase_return'
    >>> action.amount
    Decimal('25.00')
    >>> complaint.save()
    >>> complaint.state
    'draft'
    >>> complaint.click('wait')
    >>> complaint.state
    'waiting'
    >>> complaint.click('approve')
    >>> complaint.click('process')
    >>> complaint.state
    'done'
    >>> action, = complaint.actions
    >>> return_purchase = action.result
    >>> len(return_purchase.lines)
    2
    >>> sum(l.quantity for l in return_purchase.lines)
    -5.0

Create a complaint to return some of the purchase::

    >>> complaint = Complaint()
    >>> complaint.supplier = supplier
    >>> complaint.type = purchase_type
    >>> complaint.origin = purchase
    >>> action = complaint.actions.new()
    >>> action.action = 'purchase_return'
    >>> purchase_line = action.purchase_lines.new()
    >>> purchase_line.line = purchase.lines[0]
    >>> purchase_line.quantity = 1
    >>> purchase_line.unit_price = Decimal('5')
    >>> purchase_line = action.purchase_lines.new()
    >>> purchase_line.line = purchase.lines[1]
    >>> action.amount
    Decimal('15.00')
    >>> complaint.save()
    >>> complaint.state
    'draft'
    >>> complaint.click('wait')
    >>> complaint.state
    'waiting'
    >>> complaint.click('approve')
    >>> complaint.state
    'done'
    >>> action, = complaint.actions
    >>> return_purchase = action.result
    >>> len(return_purchase.lines)
    2
    >>> sum(l.quantity for l in return_purchase.lines)
    -3.0
    >>> return_purchase.total_amount
    Decimal('-15.00')

Create a complaint to return a purchase line::

    >>> complaint = Complaint()
    >>> complaint.supplier = supplier
    >>> complaint.type = purchase_line_type
    >>> complaint.origin = purchase.lines[0]
    >>> action = complaint.actions.new()
    >>> action.action = 'purchase_return'
    >>> action.quantity = 1
    >>> action.amount
    Decimal('5.00')
    >>> complaint.click('wait')
    >>> complaint.click('approve')
    >>> complaint.state
    'done'
    >>> action, = complaint.actions
    >>> return_purchase = action.result
    >>> return_line, = return_purchase.lines
    >>> return_line.quantity
    -1.0

Create a complaint to credit the invoice::

    >>> complaint = Complaint()
    >>> complaint.supplier = supplier
    >>> complaint.type = invoice_type
    >>> complaint.origin = invoice
    >>> action = complaint.actions.new()
    >>> action.action = 'credit_note'
    >>> action.amount
    Decimal('25.00')
    >>> complaint.click('wait')
    >>> complaint.click('approve')
    >>> complaint.click('process')
    >>> complaint.state
    'done'
    >>> action, = complaint.actions
    >>> credit_note = action.result
    >>> credit_note.type
    'in'
    >>> len(credit_note.lines)
    4
    >>> sum(l.quantity for l in credit_note.lines)
    0.0
    >>> credit_note.total_amount
    Decimal('-25.00')

Create a complaint to credit some of the invoice::

    >>> complaint = Complaint()
    >>> complaint.supplier = supplier
    >>> complaint.type = invoice_type
    >>> complaint.origin = invoice
    >>> action = complaint.actions.new()
    >>> action.action = 'credit_note'
    >>> invoice_line = action.invoice_lines.new()
    >>> invoice_line.line = invoice.lines[0]
    >>> invoice_line.quantity = 1
    >>> invoice_line.unit_price = Decimal('5')
    >>> invoice_line = action.invoice_lines.new()
    >>> invoice_line.line = invoice.lines[1]
    >>> invoice_line.quantity = 1
    >>> action.amount
    Decimal('10.00')
    >>> complaint.click('wait')
    >>> complaint.click('approve')
    >>> complaint.state
    'done'
    >>> action, = complaint.actions
    >>> credit_note = action.result
    >>> credit_note.type
    'in'
    >>> len(credit_note.lines)
    4
    >>> sum(l.quantity for l in credit_note.lines)
    0.0
    >>> credit_note.total_amount
    Decimal('-10.00')

Create a complaint to credit an invoice line::

    >>> complaint = Complaint()
    >>> complaint.supplier = supplier
    >>> complaint.type = invoice_line_type
    >>> complaint.origin = invoice.lines[0]
    >>> action = complaint.actions.new()
    >>> action.action = 'credit_note'
    >>> action.quantity = 1
    >>> action.amount
    Decimal('5.00')
    >>> complaint.click('wait')
    >>> complaint.click('approve')
    >>> complaint.state
    'done'
    >>> action, = complaint.actions
    >>> credit_note = action.result
    >>> credit_note.type
    'in'
    >>> len(credit_note.lines)
    2
    >>> sum(l.quantity for l in credit_note.lines)
    0.0

Create a complaint to scrap products::

    >>> complaint = Complaint()
    >>> complaint.supplier = supplier
    >>> complaint.type = purchase_type
    >>> complaint.origin = purchase
    >>> action = complaint.actions.new()
    >>> action.action = 'scrap_goods'
    >>> action.amount
    Decimal('25.00')
    >>> action.scrap_location = lost_found_loc
    >>> complaint.click('wait')
    >>> complaint.click('approve')
    >>> complaint.state
    'done'
    >>> action, = complaint.actions
    >>> internal_shipment = action.result
    >>> internal_shipment.state
    'waiting'
    >>> internal_shipment.from_location == storage_loc
    True
    >>> internal_shipment.to_location == lost_found_loc
    True
    >>> len(internal_shipment.moves)
    2
    >>> sum(m.quantity for m in internal_shipment.moves)
    5.0

Create a complaint to scrap some of the purchase::

    >>> complaint = Complaint()
    >>> complaint.supplier = supplier
    >>> complaint.type = purchase_type
    >>> complaint.origin = purchase
    >>> action = complaint.actions.new()
    >>> action.action = 'scrap_goods'
    >>> action.scrap_location = lost_found_loc
    >>> purchase_line = action.purchase_lines.new()
    >>> purchase_line.line = purchase.lines[0]
    >>> purchase_line.quantity = 1
    >>> purchase_line.unit_price = Decimal('5')
    >>> purchase_line = action.purchase_lines.new()
    >>> purchase_line.line = purchase.lines[1]
    >>> action.amount
    Decimal('15.00')
    >>> complaint.save()
    >>> complaint.state
    'draft'
    >>> complaint.click('wait')
    >>> complaint.state
    'waiting'
    >>> complaint.click('approve')
    >>> complaint.state
    'done'
    >>> action, = complaint.actions
    >>> internal_shipment = action.result
    >>> internal_shipment.state
    'waiting'
    >>> len(internal_shipment.moves)
    2
    >>> sum(m.quantity for m in internal_shipment.moves)
    3.0

Create a complaint to return a purchase line::

    >>> complaint = Complaint()
    >>> complaint.supplier = supplier
    >>> complaint.type = purchase_line_type
    >>> complaint.origin = purchase.lines[0]
    >>> action = complaint.actions.new()
    >>> action.action = 'scrap_goods'
    >>> action.scrap_location = lost_found_loc
    >>> action.quantity = 1
    >>> action.amount
    Decimal('5.00')
    >>> complaint.click('wait')
    >>> complaint.click('approve')
    >>> complaint.state
    'done'
    >>> action, = complaint.actions
    >>> internal_shipment = action.result
    >>> internal_shipment.state
    'waiting'
    >>> move, = internal_shipment.moves
    >>> move.quantity
    1.0
