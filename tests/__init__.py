# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

try:
    from trytond.modules.purchase_complaint.tests.test_purchase_complaint import suite  # noqa: E501
except ImportError:
    from .test_purchase_complaint import suite

__all__ = ['suite']
