# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.pyson import Eval, Id


class Configuration(metaclass=PoolMeta):
    __name__ = 'purchase.configuration'

    complaint_sequence = fields.MultiValue(fields.Many2One(
            'ir.sequence', "Complaint Sequence",
            domain=[
                ('sequence_type', '=', Id('purchase_complaint',
                    'sequence_type_complaint')),
                ('company', 'in', [
                        Eval('context', {}).get('company', -1), None]),
                ]))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'complaint_sequence':
            return pool.get('purchase.configuration.sequence')
        return super().multivalue_model(field)

    @classmethod
    def default_complaint_sequence(cls, **pattern):
        return cls.multivalue_model(
            'complaint_sequence').default_complaint_sequence()


class ConfigurationSequence(metaclass=PoolMeta):
    __name__ = 'purchase.configuration.sequence'
    complaint_sequence = fields.Many2One(
        'ir.sequence', "Complaint Sequence",
        domain=[
            ('sequence_type', '=', Id('purchase_complaint',
                'sequence_type_complaint')),
            ('company', 'in', [Eval('company', -1), None]),
            ],
        depends=['company'])

    @classmethod
    def default_complaint_sequence(cls):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        try:
            return ModelData.get_id('purchase_complaint', 'sequence_complaint')
        except KeyError:
            return None


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    @classmethod
    def _get_origin(cls):
        return super()._get_origin() + ['purchase.complaint']
